FROM fedora:33
RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf && \
    echo "install_weak_deps=false" >> /etc/dnf/dnf.conf && \
    dnf -y upgrade && \
    dnf -y install buildah \
                   git \
                   git-lfs \
                   podman \
                   runc && \
    dnf clean all
